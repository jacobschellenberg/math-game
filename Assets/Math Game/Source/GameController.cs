﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour {

  public OperatorsViewController operatorsViewController;

  static private GameController instance;

  static public GameController Instance
  {
    get
    {
      if (instance == null)
      {
        instance = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        //instance = new GameObject("Game Controller", typeof(GameController)).GetComponent<GameController>();
      }

      return instance;
    }
  }

  public void OperatorButtonClicked(UIButton button)
  {
    operatorsViewController.OperatorButtonClicked(button);
  }
}
