﻿using UnityEngine;
using System.Collections;

public class OperatorButton : MonoBehaviour {

  private void OnClick() {
    GameController.Instance.OperatorButtonClicked(GetComponent<UIButton>());
  }
}
