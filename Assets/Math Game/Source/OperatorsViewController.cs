﻿using UnityEngine;
using System.Collections;

public class OperatorsViewController : MonoBehaviour {

  public UIButton addButton;
  public UIButton subtractButton;
  public UIButton multiplyButton;
  public UIButton divideButton;

  public void OperatorButtonClicked(UIButton button) {
    Debug.Log(button.name + " clicked.");
  }
}
